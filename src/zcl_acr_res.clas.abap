CLASS zcl_acr_res DEFINITION
  PUBLIC
  INHERITING FROM cl_adt_rest_resource
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS constructor.

    METHODS get REDEFINITION.
    METHODS post REDEFINITION.
  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA m_review_data TYPE REF TO zcl_acr_review_data.
    DATA m_auth_controller TYPE REF TO zcl_acr_review_auth_controller.
    DATA m_content_handler TYPE REF TO if_adt_rest_content_handler.

ENDCLASS.



CLASS zcl_acr_res IMPLEMENTATION.

  METHOD constructor.
    super->constructor( ).
    " TODO: Make injectable
    m_review_data = NEW #( ).
    m_auth_controller = NEW #( ).
    m_content_handler = zcl_acr_rest_content_handler=>m_instance.
  ENDMETHOD.

  METHOD get.
    DATA lv_review_id TYPE zacr_review_id.
    request->get_uri_attribute( EXPORTING name = 'review_id' mandatory = abap_false IMPORTING value = lv_review_id ).

    IF lv_review_id IS INITIAL.
      " TODO: This can be a join between ZACR_REVIEW and ZACR_PEOPLE
      DATA(lt_reviews) = m_review_data->get_reviews( ).
      LOOP AT lt_reviews ASSIGNING FIELD-SYMBOL(<review>).
        IF NOT m_auth_controller->can_display_review( iv_review_id = <review>-review_id iv_username = sy-uname ).
          DELETE lt_reviews.
        ENDIF.
      ENDLOOP.

      response->set_body_data(
          content_handler = m_content_handler
          data = VALUE zacr_reviews_response(
              reviews = lt_reviews
          )
      ).
    ELSE.

      IF NOT m_review_data->review_id_exists( lv_review_id ).
        RAISE EXCEPTION TYPE cx_adt_res_not_found.
      ENDIF.

      IF NOT m_auth_controller->can_display_review( iv_review_id = lv_review_id iv_username = sy-uname ).
        RAISE EXCEPTION TYPE cx_adt_res_no_authorization.
      ENDIF.

      DATA(lt_people) = m_review_data->get_review_people( lv_review_id ).

      response->set_body_data(
          content_handler = m_content_handler
          data = VALUE zacr_review_response(
              review    = m_review_data->get_review( lv_review_id )
              lines     = m_review_data->get_review_lines( lv_review_id )
              reviewers = VALUE #( FOR <row> IN lt_people WHERE ( is_reviewer = abap_true ) ( <row>-user_id ) )
              audience  = VALUE #( FOR <row> IN lt_people WHERE ( is_reviewer <> abap_true ) ( <row>-user_id ) )
          )
      ).
    ENDIF.
  ENDMETHOD.

  METHOD post.
    DATA ls_request TYPE zacr_reviews_create_request.
    request->get_body_data(
      EXPORTING
        content_handler = zcl_acr_rest_content_handler=>m_instance
      IMPORTING
        data            = ls_request
    ).

    DATA(ls_review) = VALUE zacr_review_head(
        BASE CORRESPONDING #( ls_request )
        created_by = sy-uname
    ).
    GET TIME STAMP FIELD ls_review-created_on.

    TRY.
        m_review_data->create_review( CHANGING cs_review = ls_review ).
        m_review_data->create_review_person( iv_review_id = ls_review-review_id iv_user_id = sy-uname iv_is_reviewer = abap_true ).
      CATCH zcx_acr_db_access INTO DATA(cx).
        RAISE EXCEPTION TYPE cx_adt_res_creation_failure EXPORTING previous = cx.
    ENDTRY.

    DATA(lt_people) = m_review_data->get_review_people( ls_review-review_id ).

    response->set_body_data(
      content_handler = m_content_handler
      data = VALUE zacr_review_response(
        review    = ls_review
        lines     = VALUE #( )
        reviewers = VALUE #( FOR <row> IN lt_people WHERE ( is_reviewer = abap_true ) ( <row>-user_id ) )
        audience  = VALUE #( FOR <row> IN lt_people WHERE ( is_reviewer <> abap_true ) ( <row>-user_id ) )
      )
    ).
  ENDMETHOD.

ENDCLASS.
