CLASS zcl_acr_res_app DEFINITION
  PUBLIC
  INHERITING FROM cl_adt_disc_res_app_base
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS:
      if_adt_rest_rfc_application~get_static_uri_path REDEFINITION.
  PROTECTED SECTION.
    METHODS:
      get_application_title REDEFINITION,
      register_resources REDEFINITION.
  PRIVATE SECTION.
ENDCLASS.



CLASS zcl_acr_res_app IMPLEMENTATION.
  METHOD get_application_title.
    result = 'ADT Code Review'.
  ENDMETHOD.

  METHOD register_resources.
    DATA(lv_collection) = registry->register_discoverable_resource(
        url             = '/reviews'
        handler_class   = 'ZCL_ACR_RES'
        description     = 'Reviews'
        category_term   = 'reviews'
        category_scheme = 'http://founderio.net/acr/reviews'
    ).
    lv_collection->register_disc_res_w_template(
        relation        = 'http://founderio.net/acr/review'
        template        = '/reviews/{review_id}'
        description     = 'Review'
        handler_class   = 'ZCL_ACR_RES'
    ).
    lv_collection->register_disc_res_w_template(
        relation        = 'http://founderio.net/acr/review'
        template        = '/reviews/{review_id}/lines'
        description     = 'Review Lines'
        handler_class   = 'ZCL_ACR_RES_LINE'
    ).
    lv_collection->register_disc_res_w_template(
        relation        = 'http://founderio.net/acr/review'
        template        = '/reviews/{review_id}/lines/{line_id}'
        description     = 'Review Line'
        handler_class   = 'ZCL_ACR_RES_LINE'
    ).
    lv_collection->register_disc_res_w_template(
        relation        = 'http://founderio.net/acr/review_line'
        template        = '/reviews/{review_id}/lines/{line_id}/completed'
        description     = 'Review Line Completed'
        handler_class   = 'ZCL_ACR_RES_LINE_CONF'
    ).
    lv_collection->register_disc_res_w_template(
        relation        = 'http://founderio.net/acr/review_line'
        template        = '/reviews/{review_id}/lines/{line_id}/assignee'
        description     = 'Review Line Assignee'
        handler_class   = 'ZCL_ACR_RES_LINE_ASSIGN'
    ).
    lv_collection->register_disc_res_w_template(
        relation        = 'http://founderio.net/acr/people'
        template        = '/reviews/{review_id}/people'
        description     = 'Review Reviewers and Audience'
        handler_class   = 'ZCL_ACR_RES_PEOPLE'
    ).
    lv_collection->register_disc_res_w_template(
        relation        = 'http://founderio.net/acr/people'
        template        = '/reviews/{review_id}/people/{role}'
        description     = 'Review Reviewers and Audience'
        handler_class   = 'ZCL_ACR_RES_PEOPLE'
    ).
    lv_collection->register_disc_res_w_template(
        relation        = 'http://founderio.net/acr/people'
        template        = '/reviews/{review_id}/people/{role}/{username}'
        description     = 'Review Reviewers and Audience'
        handler_class   = 'ZCL_ACR_RES_PEOPLE'
    ).


  ENDMETHOD.

  METHOD if_adt_rest_rfc_application~get_static_uri_path.
    result = '/zacr'.
  ENDMETHOD.

ENDCLASS.
