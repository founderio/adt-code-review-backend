CLASS zcl_acr_res_line DEFINITION
  PUBLIC
  INHERITING FROM cl_adt_rest_resource
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS constructor.

    METHODS post REDEFINITION.
    METHODS delete REDEFINITION.
  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA m_review_data TYPE REF TO zcl_acr_review_data.
    DATA m_auth_controller TYPE REF TO zcl_acr_review_auth_controller.
    DATA m_content_handler TYPE REF TO if_adt_rest_content_handler.

ENDCLASS.



CLASS zcl_acr_res_line IMPLEMENTATION.

  METHOD constructor.
    super->constructor( ).
    " TODO: Make injectable
    m_review_data = NEW #( ).
    m_auth_controller = NEW #( ).
    m_content_handler = zcl_acr_rest_content_handler=>m_instance.
  ENDMETHOD.

  METHOD post.
    DATA:
      lv_review_id TYPE zacr_review_id,
      lv_line_id   TYPE zacr_review_line_id.
    request->get_uri_attribute( EXPORTING name = 'review_id' mandatory = abap_true IMPORTING value = lv_review_id ).
    request->get_uri_attribute( EXPORTING name = 'line_id' mandatory = abap_false IMPORTING value = lv_line_id ).

    DATA ls_request TYPE zacr_review_lines_create_req.
    request->get_body_data(
      EXPORTING
        content_handler = zcl_acr_rest_content_handler=>m_instance
      IMPORTING
        data            = ls_request
    ).

    IF NOT m_review_data->review_id_exists( lv_review_id ).
      RAISE EXCEPTION TYPE cx_adt_res_not_found.
    ENDIF.

    IF NOT m_auth_controller->can_edit_review( iv_review_id = lv_review_id iv_username = sy-uname ).
      RAISE EXCEPTION TYPE cx_adt_res_no_authorization.
    ENDIF.

    DATA(ls_review_line) = VALUE zacr_review_line(
        BASE CORRESPONDING #( ls_request )
        review_id = lv_review_id
        line_id = lv_line_id
    ).
*  TODO: Creation/Update timestamp  GET TIME STAMP FIELD ls_review-created_on.

    IF lv_line_id IS INITIAL.
      TRY.
          m_review_data->create_review_line( CHANGING cs_review_line = ls_review_line ).
        CATCH zcx_acr_db_access INTO DATA(cx).
          RAISE EXCEPTION TYPE cx_adt_res_creation_failure EXPORTING previous = cx.
      ENDTRY.
    ELSE.
      TRY.
          m_review_data->update_review_line( CHANGING cs_review_line = ls_review_line ).
        CATCH zcx_acr_db_access INTO DATA(cx2).
          RAISE EXCEPTION TYPE cx_adt_res_creation_failure EXPORTING previous = cx2.
      ENDTRY.
    ENDIF.

    response->set_body_data(
      content_handler = m_content_handler
      data = VALUE zacr_review_line_response(
        line = ls_review_line
      )
    ).
  ENDMETHOD.

  METHOD delete.
    DATA:
      lv_review_id TYPE zacr_review_id,
      lv_line_id   TYPE zacr_review_line_id.
    request->get_uri_attribute( EXPORTING name = 'review_id' mandatory = abap_true IMPORTING value = lv_review_id ).
    request->get_uri_attribute( EXPORTING name = 'line_id' mandatory = abap_true IMPORTING value = lv_line_id ).

    IF NOT m_review_data->review_id_exists( lv_review_id ).
      RAISE EXCEPTION TYPE cx_adt_res_not_found.
    ENDIF.

    IF NOT m_auth_controller->can_edit_review( iv_review_id = lv_review_id iv_username = sy-uname ).
      RAISE EXCEPTION TYPE cx_adt_res_no_authorization.
    ENDIF.

    TRY.
        m_review_data->delete_review_line( iv_review_id = lv_review_id iv_line_id = lv_line_id ).
      CATCH zcx_acr_db_access INTO DATA(cx).
        RAISE EXCEPTION TYPE cx_adt_res_deletion_failure EXPORTING previous = cx.
    ENDTRY.
  ENDMETHOD.

ENDCLASS.
