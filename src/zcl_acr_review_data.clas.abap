CLASS zcl_acr_review_data DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    CONSTANTS:
      mc_review_number_range_number TYPE nrnr VALUE 'Z1',
      mc_review_number_range_object TYPE nrobj VALUE 'ZACR',
      "! Number of attempts made to get a number from number range.
      "! If the number collides with existing DB values this many times,
      "! an exception is raised.
      mc_review_number_range_limit  TYPE i VALUE 10,

      mc_review_id_base34_alphabet  TYPE string VALUE '123456789ABCDEFGHJKLMNPQRSTUVWXYZ',
      mc_review_id_base34           TYPE i VALUE 34.

    CLASS-METHODS:
      convert_numeric_id
        IMPORTING iv_review_id_numeric TYPE zacr_review_id_numeric
        RETURNING VALUE(rv_review_id)  TYPE zacr_review_id.

    METHODS:
      "! <p class="shorttext synchronized" lang="en">Create a new review</p>
      "! Creates is_review on DB in table ZACR_REVIEW_HEAD and assigns the generated ID
      "! to the field review_id of is_review.
      "! @parameter cs_review | <p class="shorttext synchronized" lang="en">Review to be created. Field review_id will be assigned.</p>
      "! @raising zcx_acr_db_access | <p class="shorttext synchronized" lang="en">Raised on DB error, review ID assignment error</p>
      create_review
        CHANGING cs_review TYPE zacr_review_head
        RAISING  zcx_acr_db_access,
      "! <p class="shorttext synchronized" lang="en">Check if a review exists on DB</p>
      "!
      "! @parameter iv_review_id | <p class="shorttext synchronized" lang="en">The review ID to be checked</p>
      "! @parameter rv_exists | <p class="shorttext synchronized" lang="en">abap_true if the review exists, abap_false if not</p>
      review_id_exists
        IMPORTING iv_review_id     TYPE zacr_review_id
        RETURNING VALUE(rv_exists) TYPE abap_bool,
      "! <p class="shorttext synchronized" lang="en">Get a review from DB</p>
      "!
      "! @parameter iv_review_id | <p class="shorttext synchronized" lang="en">Review ID to load from DB</p>
      "! @parameter rs_review | <p class="shorttext synchronized" lang="en">Review with the given ID, or initial if it does not exists</p>
      get_review
        IMPORTING iv_review_id     TYPE zacr_review_id
        RETURNING VALUE(rs_review) TYPE zacr_review_head,
      "! <p class="shorttext synchronized" lang="en">Get lines of a review from DB</p>
      "!
      "! @parameter iv_review_id | <p class="shorttext synchronized" lang="en">Review ID to load from DB</p>
      "! @parameter rt_people | <p class="shorttext synchronized" lang="en">Review people for the given DB, or initial if none exist</p>
      get_review_people
        IMPORTING iv_review_id     TYPE zacr_review_id
        RETURNING VALUE(rt_people) TYPE zacr_people_t,
      "! <p class="shorttext synchronized" lang="en">Get lines of a review from DB</p>
      "!
      "! @parameter iv_review_id | <p class="shorttext synchronized" lang="en">Review ID to load from DB</p>
      "! @parameter rt_lines | <p class="shorttext synchronized" lang="en">Review lines for the given DB, or initial if none exist</p>
      get_review_lines
        IMPORTING iv_review_id    TYPE zacr_review_id
        RETURNING VALUE(rt_lines) TYPE zacr_review_line_t,
      "! <p class="shorttext synchronized" lang="en">Get a line of a review from DB</p>
      "!
      "! @parameter iv_review_id | <p class="shorttext synchronized" lang="en">Review ID to load from DB</p>
      "! @parameter rs_line | <p class="shorttext synchronized" lang="en">Review line for the given ids, or initial if none exist</p>
      get_review_line
        IMPORTING iv_review_id   TYPE zacr_review_id
                  iv_line_id     TYPE zacr_review_line_id
        RETURNING VALUE(rs_line) TYPE zacr_review_line,
      "! <p class="shorttext synchronized" lang="en">Get all reviews from DB</p>
      "!
      "! @parameter rt_reviews | <p class="shorttext synchronized" lang="en">All reviews on DB, or initial if none exists</p>
      get_reviews
        RETURNING VALUE(rt_reviews) TYPE zacr_review_head_t,
      "! <p class="shorttext synchronized" lang="en">Create a new review line</p>
      "! Create a review line on DB and fills the line ID in the parameter structure.
      "! Expects the structure to have the review ID field filled.
      "!
      "! @parameter cs_review_line | <p class="shorttext synchronized" lang="en">Review line to be created. Line ID will be filled.</p>
      "! @raising zcx_acr_db_access | <p class="shorttext synchronized" lang="en">Raised on DB access error, or if review ID does not exist</p>
      create_review_line
        CHANGING cs_review_line TYPE zacr_review_line
        RAISING  zcx_acr_db_access,
      update_review_line
        CHANGING cs_review_line TYPE zacr_review_line
        RAISING  zcx_acr_db_access,
      "! <p class="shorttext synchronized" lang="en"></p>
      "! Assumes the review exists on DB (check separately!)
      "! Raises an exception if the line to be deleted does not exist.
      "!
      "! @parameter iv_review_id | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter iv_line_id | <p class="shorttext synchronized" lang="en"></p>
      "! @raising zcx_acr_db_access | <p class="shorttext synchronized" lang="en"></p>
      delete_review_line
        IMPORTING iv_review_id TYPE zacr_review_id
                  iv_line_id   TYPE zacr_review_line_id
        RAISING   zcx_acr_db_access,
      "! <p class="shorttext synchronized" lang="en">Set or remove the assignee of a review line</p>
      "!
      "! @parameter iv_review_id | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter iv_line_id | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter iv_assignee | <p class="shorttext synchronized" lang="en"></p>
      "! @raising zcx_acr_db_access | <p class="shorttext synchronized" lang="en"></p>
      set_review_line_assignee
        IMPORTING iv_review_id TYPE zacr_review_id
                  iv_line_id   TYPE zacr_review_line_id
                  iv_assignee  TYPE zacr_assignee
        RAISING   zcx_acr_db_access,
      "! <p class="shorttext synchronized" lang="en">Set or remove the confirmed flag of a review line</p>
      "!
      "! @parameter iv_review_id | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter iv_line_id | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter iv_complete | <p class="shorttext synchronized" lang="en"></p>
      "! @raising zcx_acr_db_access | <p class="shorttext synchronized" lang="en"></p>
      set_review_line_completed
        IMPORTING iv_review_id TYPE zacr_review_id
                  iv_line_id   TYPE zacr_review_line_id
                  iv_complete  TYPE zacr_completed
        RAISING   zcx_acr_db_access,
      delete_review_person
        IMPORTING iv_review_id TYPE zacr_review_id
                  iv_user_id   TYPE uname
        RAISING   zcx_acr_db_access,
      create_review_person
        IMPORTING iv_review_id   TYPE zacr_review_id
                  iv_user_id     TYPE uname
                  iv_is_reviewer TYPE abap_bool
        RAISING   zcx_acr_db_access,
      "! <p class="shorttext synchronized" lang="en"></p>
      "! Assumes the review exists on DB (check separately!)
      "!
      "! @parameter iv_review_id | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter iv_user_id | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter rs_person | <p class="shorttext synchronized" lang="en"></p>
      get_review_person
        IMPORTING iv_review_id     TYPE zacr_review_id
                  iv_user_id       TYPE uname
        RETURNING VALUE(rs_person) TYPE zacr_people.
  PROTECTED SECTION.
  PRIVATE SECTION.
    "! <p class="shorttext synchronized" lang="en">Assign a new review ID from number range</p>
    "!
    "! @parameter rv_review_id | <p class="shorttext synchronized" lang="en">A review ID which does not exist on DB yet</p>
    "! @raising zcx_acr_db_access | <p class="shorttext synchronized" lang="en">Raised when number range is exhausted or out of sync</p>
    METHODS assign_review_id
      RETURNING
        VALUE(rv_review_id) TYPE zacr_review_id
      RAISING
        zcx_acr_db_access.
ENDCLASS.



CLASS zcl_acr_review_data IMPLEMENTATION.

  METHOD convert_numeric_id.
    rv_review_id = /ui2/cl_number=>base_converter( number = iv_review_id_numeric to = mc_review_id_base34 alphabet = mc_review_id_base34_alphabet ).
  ENDMETHOD.

  METHOD create_review.
    cs_review-review_id = assign_review_id( ).

    INSERT zacr_review_head FROM cs_review.
    IF sy-subrc <> 0.
      " Error creating review on DB
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e901(zacr).
    ENDIF.

  ENDMETHOD.

  METHOD assign_review_id.

    DATA lv_review_id_numeric TYPE zacr_review_id_numeric.

    DO mc_review_number_range_limit TIMES.
      CALL FUNCTION 'NUMBER_GET_NEXT'
        EXPORTING
          nr_range_nr             = mc_review_number_range_number
          object                  = mc_review_number_range_object
        IMPORTING
          number                  = lv_review_id_numeric
        EXCEPTIONS
          interval_not_found      = 1
          number_range_not_intern = 2
          object_not_found        = 3
          quantity_is_0           = 4
          quantity_is_not_1       = 5
          interval_overflow       = 6
          buffer_overflow         = 7
          OTHERS                  = 8.
      IF sy-subrc <> 0.
        RAISE EXCEPTION TYPE zcx_acr_db_access
          MESSAGE ID sy-msgid TYPE 'E' NUMBER sy-msgno WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ENDIF.

      rv_review_id = convert_numeric_id( lv_review_id_numeric ).

      " Check for collisions on DB, in case number range is out of sync.
      " If we found a new ID, exit retry-loop and return the ID we found.
      IF NOT review_id_exists( rv_review_id ).
        RETURN.
      ENDIF.

    ENDDO.

    " Too many Review ID collisions in ZACR_REVIEW_HEAD, check number range
    RAISE EXCEPTION TYPE zcx_acr_db_access
    MESSAGE e902(zacr).

  ENDMETHOD.



  METHOD review_id_exists.
    SELECT SINGLE @abap_true
      FROM zacr_review_head
      INTO @rv_exists
     WHERE review_id = @iv_review_id.
    IF sy-subrc <> 0.
      CLEAR rv_exists.
    ENDIF.
  ENDMETHOD.

  METHOD get_review.
    SELECT SINGLE *
      FROM zacr_review_head
      INTO @rs_review
     WHERE review_id = @iv_review_id.
    IF sy-subrc <> 0.
      CLEAR rs_review.
    ENDIF.
  ENDMETHOD.

  METHOD get_reviews.
    SELECT * FROM zacr_review_head
    WHERE lvorm = ''
    INTO TABLE @rt_reviews.
    IF sy-subrc <> 0.
      CLEAR rt_reviews.
    ENDIF.
  ENDMETHOD.

  METHOD get_review_people.
    SELECT * FROM zacr_people
     WHERE review_id = @iv_review_id
     ORDER BY review_id ASCENDING, is_reviewer DESCENDING, user_id ASCENDING
      INTO TABLE @rt_people.
    IF sy-subrc <> 0.
      CLEAR rt_people.
    ENDIF.
  ENDMETHOD.

  METHOD get_review_lines.
    SELECT * FROM zacr_review_line
    WHERE review_id = @iv_review_id
    ORDER BY line_id ASCENDING
    INTO TABLE @rt_lines.
    IF sy-subrc <> 0.
      CLEAR rt_lines.
    ENDIF.
  ENDMETHOD.

  METHOD get_review_line.
    SELECT SINGLE * FROM zacr_review_line
    WHERE review_id = @iv_review_id
      AND line_id = @iv_line_id
    INTO @rs_line.
    IF sy-subrc <> 0.
      CLEAR rs_line.
    ENDIF.
  ENDMETHOD.

  METHOD create_review_line.
    IF NOT review_id_exists( cs_review_line-review_id ).
      " Review with the ID '&1' does not exist
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e801(zacr) WITH cs_review_line-review_id.
    ENDIF.

    SELECT SINGLE MAX( line_id + 1 ) AS next_line_id
    FROM zacr_review_line
    WHERE review_id = @cs_review_line-review_id
    INTO @cs_review_line-line_id.
    IF sy-subrc <> 0.
      " Error creating review line on DB
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e903(zacr).
    ENDIF.

    " If there are no existing rows, the above select returns 0.
    " We want to start numbering at 1.
    IF cs_review_line-line_id = 0.
      cs_review_line-line_id = 1.
    ENDIF.

    INSERT zacr_review_line FROM cs_review_line.
    IF sy-subrc <> 0.
      " Error creating review line on DB
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e903(zacr).
    ENDIF.

  ENDMETHOD.

  METHOD update_review_line.
    IF NOT review_id_exists( cs_review_line-review_id ).
      " Review with the ID '&1' does not exist
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e801(zacr) WITH cs_review_line-review_id.
    ENDIF.

    SELECT SINGLE @abap_true FROM zacr_review_line
    WHERE review_id = @cs_review_line-review_id
    AND line_id = @cs_review_line-line_id
    INTO @DATA(lv_exists).
    IF sy-subrc <> 0 OR lv_exists <> abap_true.
      " Review line '&1' does not exist in review '&2'
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e802(zacr) WITH cs_review_line-line_id cs_review_line-review_id.
    ENDIF.

    MODIFY zacr_review_line
    FROM cs_review_line.
    IF sy-subrc <> 0.
      " Error creating review line on DB
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e903(zacr).
    ENDIF.

  ENDMETHOD.

  METHOD delete_review_line.
    DELETE FROM zacr_review_line
    WHERE review_id = @iv_review_id
    AND line_id = @iv_line_id.
    IF sy-subrc <> 0.
      " Review line '&1' does not exist in review '&2'
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e802(zacr) WITH iv_line_id iv_review_id.
    ENDIF.

  ENDMETHOD.

  METHOD set_review_line_assignee.

    UPDATE zacr_review_line
       SET assignee = @iv_assignee
     WHERE review_id = @iv_review_id
       AND line_id = @iv_line_id.

    IF sy-subrc <> 0.
      " Review line '&1' does not exist in review '&2'
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e802(zacr) WITH iv_line_id iv_review_id.
    ENDIF.

  ENDMETHOD.

  METHOD set_review_line_completed.

    UPDATE zacr_review_line
       SET completed = @iv_complete
     WHERE review_id = @iv_review_id
       AND line_id = @iv_line_id.

    IF sy-subrc <> 0.
      " Review line '&1' does not exist in review '&2'
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e802(zacr) WITH iv_line_id iv_review_id.
    ENDIF.
  ENDMETHOD.

  METHOD delete_review_person.
    IF NOT review_id_exists( iv_review_id ).
      " Review with the ID '&1' does not exist
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e801(zacr) WITH iv_review_id.
    ENDIF.

    DELETE FROM zacr_people
    WHERE review_id = @iv_review_id
    AND user_id = @iv_user_id.
    IF sy-subrc <> 0.
      " Username '&1' is not assigned to review '&2'
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e802(zacr) WITH iv_user_id iv_review_id.
    ENDIF.

  ENDMETHOD.

  METHOD create_review_person.
    IF NOT review_id_exists( iv_review_id ).
      " Review with the ID '&1' does not exist
      RAISE EXCEPTION TYPE zcx_acr_db_access
      MESSAGE e801(zacr) WITH iv_review_id.
    ENDIF.

    DATA(ls_row) = VALUE zacr_people(
        review_id = iv_review_id
        user_id = iv_user_id
        is_reviewer = iv_is_reviewer
    ).

    MODIFY zacr_people FROM @ls_row.

  ENDMETHOD.

  METHOD get_review_person.
    SELECT SINGLE * FROM zacr_people
     WHERE review_id = @iv_review_id
       AND user_id = @iv_user_id
      INTO @rs_person.
    IF sy-subrc <> 0.
      CLEAR rs_person.
    ENDIF.
  ENDMETHOD.

ENDCLASS.
