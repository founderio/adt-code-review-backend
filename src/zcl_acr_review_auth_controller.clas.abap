CLASS zcl_acr_review_auth_controller DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS constructor.

    "! <p class="shorttext synchronized" lang="en"></p>
    "! Assumes the review exists on DB (check separately!)
    "!
    "! @parameter iv_review_id | <p class="shorttext synchronized" lang="en"></p>
    "! @parameter iv_username | <p class="shorttext synchronized" lang="en"></p>
    "! @parameter rv_can_edit | <p class="shorttext synchronized" lang="en"></p>
    METHODS can_edit_review
      IMPORTING iv_review_id       TYPE zacr_review_id
                iv_username        TYPE uname
      RETURNING VALUE(rv_can_edit) TYPE abap_bool.

    "! <p class="shorttext synchronized" lang="en"></p>
    "! Assumes the review exists on DB (check separately!)
    "!
    "! @parameter iv_review_id | <p class="shorttext synchronized" lang="en"></p>
    "! @parameter iv_username | <p class="shorttext synchronized" lang="en"></p>
    "! @parameter rv_can_display | <p class="shorttext synchronized" lang="en"></p>
    METHODS can_display_review
      IMPORTING iv_review_id          TYPE zacr_review_id
                iv_username           TYPE uname
      RETURNING VALUE(rv_can_display) TYPE abap_bool.

    "! <p class="shorttext synchronized" lang="en"></p>
    "! Assumes the review exists on DB (check separately!)
    "!
    "! @parameter iv_review_id | <p class="shorttext synchronized" lang="en"></p>
    "! @parameter iv_username | <p class="shorttext synchronized" lang="en"></p>
    "! @parameter rv_can_assign | <p class="shorttext synchronized" lang="en"></p>
    METHODS can_assign_review
      IMPORTING iv_review_id         TYPE zacr_review_id
                iv_username          TYPE uname
      RETURNING VALUE(rv_can_assign) TYPE abap_bool.

  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA m_review_data TYPE REF TO zcl_acr_review_data.
ENDCLASS.



CLASS zcl_acr_review_auth_controller IMPLEMENTATION.

  METHOD constructor.
    " TODO: Make injectable
    m_review_data = NEW #( ).
  ENDMETHOD.

  METHOD can_assign_review.
    DATA(ls_person) = m_review_data->get_review_person(
      iv_review_id = iv_review_id
      iv_user_id   = iv_username
    ).
    " Either reviewer or audience can assign review lines
    rv_can_assign = xsdbool( ls_person IS NOT INITIAL ).
  ENDMETHOD.

  METHOD can_display_review.
    DATA(ls_person) = m_review_data->get_review_person(
      iv_review_id = iv_review_id
      iv_user_id   = iv_username
    ).
    " Either reviewer or audience can display reviews
    rv_can_display = xsdbool( ls_person IS NOT INITIAL ).
  ENDMETHOD.

  METHOD can_edit_review.
    DATA(ls_person) = m_review_data->get_review_person(
      iv_review_id = iv_review_id
      iv_user_id   = iv_username
    ).
    " Only reviewers can edit reviews or add additional people
    rv_can_edit = xsdbool( ls_person IS NOT INITIAL AND ls_person-is_reviewer = abap_true ).
  ENDMETHOD.

ENDCLASS.
