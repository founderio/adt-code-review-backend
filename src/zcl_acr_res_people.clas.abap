CLASS zcl_acr_res_people DEFINITION
  PUBLIC
  INHERITING FROM cl_adt_rest_resource
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS constructor.

    METHODS get REDEFINITION.
    METHODS post REDEFINITION.
    METHODS delete REDEFINITION.

    TYPES gty_role TYPE string.
    CONSTANTS:
      BEGIN OF mc_roles,
        reviewer TYPE gty_role VALUE 'reviewer',
        audience TYPE gty_role VALUE 'audience',
      END OF mc_roles.
  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA m_review_data TYPE REF TO zcl_acr_review_data.
    DATA m_auth_controller TYPE REF TO zcl_acr_review_auth_controller.
    DATA m_content_handler TYPE REF TO if_adt_rest_content_handler.
    METHODS is_valid_role
      IMPORTING
        lv_role         TYPE gty_role
      RETURNING
        VALUE(rv_valid) TYPE abap_bool.

ENDCLASS.



CLASS zcl_acr_res_people IMPLEMENTATION.

  METHOD constructor.
    super->constructor( ).
    " TODO: Make injectable
    m_review_data = NEW #( ).
    m_auth_controller = NEW #( ).
    m_content_handler = zcl_acr_rest_content_handler=>m_instance.
  ENDMETHOD.

  METHOD is_valid_role.
    rv_valid = xsdbool( lv_role = mc_roles-audience OR lv_role = mc_roles-reviewer ).
  ENDMETHOD.

  METHOD get.
    DATA:
      lv_review_id TYPE zacr_review_id,
      lv_role      TYPE gty_role.
    request->get_uri_attribute( EXPORTING name = 'review_id' mandatory = abap_true IMPORTING value = lv_review_id ).
    request->get_uri_attribute( EXPORTING name = 'role' mandatory = abap_false IMPORTING value = lv_role ).

    " Can't return specific roles (wrong path)
    IF lv_role IS NOT INITIAL.
      RAISE EXCEPTION TYPE cx_adt_res_not_found.
    ENDIF.

    IF NOT m_review_data->review_id_exists( lv_review_id ).
      RAISE EXCEPTION TYPE cx_adt_res_not_found.
    ENDIF.

    IF NOT m_auth_controller->can_display_review( iv_review_id = lv_review_id iv_username = sy-uname ).
      RAISE EXCEPTION TYPE cx_adt_res_no_authorization.
    ENDIF.

    DATA(lt_people) = m_review_data->get_review_people( lv_review_id ).

    response->set_body_data(
      content_handler = m_content_handler
      data = VALUE zacr_people_response(
        reviewers = VALUE #( FOR <row> IN lt_people WHERE ( is_reviewer = abap_true ) ( <row>-user_id ) )
        audience  = VALUE #( FOR <row> IN lt_people WHERE ( is_reviewer <> abap_true ) ( <row>-user_id ) )
      )
    ).

  ENDMETHOD.

  METHOD post.
    DATA:
      lv_review_id TYPE zacr_review_id,
      lv_role      TYPE gty_role,
      lv_username  TYPE uname.
    request->get_uri_attribute( EXPORTING name = 'review_id' mandatory = abap_true IMPORTING value = lv_review_id ).
    request->get_uri_attribute( EXPORTING name = 'role' mandatory = abap_true IMPORTING value = lv_role ).
    request->get_uri_attribute( EXPORTING name = 'username' mandatory = abap_false IMPORTING value = lv_username ).

    " Can't post to specific usernames (wrong path)
    IF lv_username IS NOT INITIAL.
      RAISE EXCEPTION TYPE cx_adt_res_not_found.
    ENDIF.

    IF NOT is_valid_role( lv_role ).
      RAISE EXCEPTION TYPE cx_adt_res_not_found.
    ENDIF.

    IF NOT m_review_data->review_id_exists( lv_review_id ).
      RAISE EXCEPTION TYPE cx_adt_res_not_found.
    ENDIF.

    DATA ls_request TYPE zacr_review_user_req.
    request->get_body_data(
      EXPORTING
        content_handler = zcl_acr_rest_content_handler=>m_instance
      IMPORTING
        data            = ls_request
    ).

    IF NOT m_auth_controller->can_edit_review( iv_review_id = lv_review_id iv_username = sy-uname ).
      RAISE EXCEPTION TYPE cx_adt_res_no_authorization.
    ENDIF.

    " TODO: Check if username exists

    DATA(lv_is_reviewer) = xsdbool( lv_role = mc_roles-reviewer ).

    DATA(ls_review_people) = m_review_data->get_review_people( lv_review_id ).

    DATA lv_already_exists TYPE abap_bool.
    IF lv_is_reviewer = abap_true.
      lv_already_exists = xsdbool( line_exists( ls_review_people[ is_reviewer = abap_true user_id = ls_request-username ] ) ).
    ELSE.
      " Consider reviewers audience, ANY line here will count
      lv_already_exists = xsdbool( line_exists( ls_review_people[ user_id = ls_request-username ] ) ).
    ENDIF.

    " Check if anything even needs to be done, skip DB update if not
    IF lv_already_exists = abap_false.
      TRY.
          m_review_data->create_review_person(
              iv_review_id      = lv_review_id
              iv_user_id        = ls_request-username
              iv_is_reviewer    = lv_is_reviewer
          ).

        CATCH zcx_acr_db_access INTO DATA(cx).
          RAISE EXCEPTION TYPE cx_adt_res_save_failure EXPORTING previous = cx.
      ENDTRY.

    ENDIF.

    DATA(lt_people) = m_review_data->get_review_people( lv_review_id ).

    response->set_body_data(
      content_handler = m_content_handler
      data = VALUE zacr_people_response(
        reviewers = VALUE #( FOR <row> IN lt_people WHERE ( is_reviewer = abap_true ) ( <row>-user_id ) )
        audience  = VALUE #( FOR <row> IN lt_people WHERE ( is_reviewer <> abap_true ) ( <row>-user_id ) )
      )
    ).

  ENDMETHOD.

  METHOD delete.
    DATA:
      lv_review_id TYPE zacr_review_id,
      lv_role      TYPE gty_role,
      lv_username  TYPE uname.
    request->get_uri_attribute( EXPORTING name = 'review_id' mandatory = abap_true IMPORTING value = lv_review_id ).
    request->get_uri_attribute( EXPORTING name = 'role' mandatory = abap_true IMPORTING value = lv_role ).
    request->get_uri_attribute( EXPORTING name = 'username' mandatory = abap_true IMPORTING value = lv_username ).

    IF NOT is_valid_role( lv_role ).
      RAISE EXCEPTION TYPE cx_adt_res_not_found.
    ENDIF.

    IF NOT m_review_data->review_id_exists( lv_review_id ).
      RAISE EXCEPTION TYPE cx_adt_res_not_found.
    ENDIF.

    IF NOT m_auth_controller->can_edit_review( iv_review_id = lv_review_id iv_username = sy-uname ).
      RAISE EXCEPTION TYPE cx_adt_res_no_authorization.
    ENDIF.

    " Ensure users can't remove themselves
    IF lv_username = sy-uname.
      RAISE EXCEPTION TYPE cx_adt_res_wrong_data.
    ENDIF.

    DATA(lv_is_reviewer) = xsdbool( lv_role = mc_roles-reviewer ).

    DATA(ls_review_people) = m_review_data->get_review_people( lv_review_id ).

    " Check if anything even needs to be done, skip DB update if not
    IF line_exists( ls_review_people[ is_reviewer = lv_is_reviewer user_id = lv_username ] ).

      TRY.
          m_review_data->delete_review_person(
              iv_review_id      = lv_review_id
              iv_user_id        = lv_username
          ).

        CATCH zcx_acr_db_access INTO DATA(cx).
          RAISE EXCEPTION TYPE cx_adt_res_save_failure EXPORTING previous = cx.
      ENDTRY.

    ENDIF.

  ENDMETHOD.

ENDCLASS.
