CLASS zcl_acr_rest_content_handler DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    CLASS-DATA m_instance TYPE REF TO if_adt_rest_content_handler.

    CLASS-METHODS class_constructor.

    METHODS constructor.

    INTERFACES if_adt_rest_content_handler.
  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA m_json TYPE REF TO /ui2/cl_json.
ENDCLASS.



CLASS zcl_acr_rest_content_handler IMPLEMENTATION.

  METHOD class_constructor.
    m_instance = NEW zcl_acr_rest_content_handler( ).
  ENDMETHOD.

  METHOD constructor.
    m_json = NEW /ui2/cl_json(
        pretty_name     = /ui2/cl_json=>pretty_mode-camel_case
        ts_as_iso8601   = /ui2/cl_json=>c_bool-true
        expand_includes = /ui2/cl_json=>c_bool-false
    ).
  ENDMETHOD.

  METHOD if_adt_rest_content_handler~deserialize.
    TRY.
        m_json->deserialize_int(
          EXPORTING
            jsonx                  = request_entity->get_binary_data( )
          CHANGING
            data                  =  data
        ).
      CATCH cx_sy_move_cast_error INTO DATA(cx).
        RAISE EXCEPTION TYPE cx_adt_rest_data_invalid EXPORTING previous = cx.
    ENDTRY.
  ENDMETHOD.

  METHOD if_adt_rest_content_handler~get_supported_content_type.
    result = if_rest_media_type=>gc_appl_json.
  ENDMETHOD.

  METHOD if_adt_rest_content_handler~serialize.
    response_entity->set_content_type( if_adt_rest_content_handler~get_supported_content_type( ) ).
    response_entity->set_string_data( m_json->serialize_int( data ) ).
  ENDMETHOD.

ENDCLASS.
