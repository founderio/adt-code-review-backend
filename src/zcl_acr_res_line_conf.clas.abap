CLASS zcl_acr_res_line_conf DEFINITION
  PUBLIC
  INHERITING FROM cl_adt_rest_resource
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS constructor.

    METHODS post REDEFINITION.
  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA m_review_data TYPE REF TO zcl_acr_review_data.
    DATA m_auth_controller TYPE REF TO zcl_acr_review_auth_controller.
    DATA m_content_handler TYPE REF TO if_adt_rest_content_handler.

ENDCLASS.



CLASS zcl_acr_res_line_conf IMPLEMENTATION.

  METHOD constructor.
    super->constructor( ).
    " TODO: Make injectable
    m_review_data = NEW #( ).
    m_auth_controller = NEW #( ).
    m_content_handler = zcl_acr_rest_content_handler=>m_instance.
  ENDMETHOD.

  METHOD post.
    DATA:
      lv_review_id TYPE zacr_review_id,
      lv_line_id   TYPE zacr_review_line_id.
    request->get_uri_attribute( EXPORTING name = 'review_id' mandatory = abap_true IMPORTING value = lv_review_id ).
    request->get_uri_attribute( EXPORTING name = 'line_id' mandatory = abap_true IMPORTING value = lv_line_id ).

    DATA ls_request TYPE zacr_review_lines_complete_req.
    request->get_body_data(
      EXPORTING
        content_handler = zcl_acr_rest_content_handler=>m_instance
      IMPORTING
        data            = ls_request
    ).

    IF NOT m_review_data->review_id_exists( lv_review_id ).
      RAISE EXCEPTION TYPE cx_adt_res_not_found.
    ENDIF.

    IF NOT m_auth_controller->can_assign_review( iv_review_id = lv_review_id iv_username = sy-uname ).
      RAISE EXCEPTION TYPE cx_adt_res_no_authorization.
    ENDIF.

    " SET or UNSET the completed flag, only allow X or blank, just to be sure...
    IF ls_request-completed <> abap_true AND ls_request-completed <> abap_false.
      RAISE EXCEPTION TYPE cx_adt_res_unprocess_entity.
    ENDIF.

    DATA(ls_review_line) = m_review_data->get_review_line(
        iv_review_id = lv_review_id
        iv_line_id = lv_line_id
    ).

    IF ls_review_line IS INITIAL.
      RAISE EXCEPTION TYPE cx_adt_res_not_found.
    ENDIF.

    " Check if anything even needs to be done, skip DB update if not
    IF ls_review_line-completed <> ls_request-completed.

      TRY.
          m_review_data->set_review_line_completed(
            iv_review_id = lv_review_id
            iv_line_id = lv_line_id
            iv_complete = ls_request-completed
          ).
        CATCH zcx_acr_db_access INTO DATA(cx).
          RAISE EXCEPTION TYPE cx_adt_res_save_failure EXPORTING previous = cx.
      ENDTRY.

      ls_review_line-completed = ls_request-completed.
    ENDIF.

    response->set_body_data(
      content_handler = m_content_handler
      data = VALUE zacr_review_line_response(
        line = ls_review_line
      )
    ).
  ENDMETHOD.

ENDCLASS.
